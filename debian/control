Source: cacti
Section: web
Priority: optional
Maintainer: Cacti Maintainer <pkg-cacti-maint@lists.alioth.debian.org>
Uploaders: Paul Gevers <elbrus@debian.org>
Build-Depends: debhelper (>= 11~),
               dh-linktree (>=0.5~),
               libjs-jquery-colorpicker,
               libjs-jquery-jstree,
               libjs-jquery-ui-theme-south-street,
               libjs-jquery-ui-theme-ui-darkness,
               libjs-jquery-ui-theme-smoothness,
               gettext,
               pandoc (>= 2),
               po-debconf,
Rules-Requires-Root: binary-targets
Homepage: https://www.cacti.net/
Vcs-Browser: https://salsa.debian.org/cacti-team/cacti
Vcs-Git: https://salsa.debian.org/cacti-team/cacti.git
Standards-Version: 4.3.0

Package: cacti
Architecture: all
Depends: dbconfig-common (>= 2.0.9~),
         dbconfig-mysql | dbconfig-no-thanks,
         fonts-dejavu-core,
         fonts-dejavu-extra,
         fonts-fork-awesome,
         javascript-common,
         libapache2-mod-php | php,
         libjs-c3,
         libjs-chart.js,
         libjs-d3,
         libjs-jquery (>= 1.10),
         libjs-jquery-cookie,
         libjs-jquery-hotkeys,
         libjs-jquery-metadata,
         libjs-jquery-tablesorter (>= 1:2.28.9+dfsg1~),
         libjs-jquery-timepicker,
         libjs-jquery-ui,
         libjs-jquery-ui-touch-punch,
         libphp-phpmailer (>= 6.0.6-0.1~),
         php-cli,
         php-gd,
         php-json,
         php-gmp,
         php-ldap,
         php-mbstring,
         php-mysql,
         php-phpseclib,
         php-snmp,
         php-twig,
         php-xml,
         rrdtool,
         snmp,
         ucf,
         ${misc:Depends},
         ${perl:Depends},
Recommends: apache2 | lighttpd | nginx | httpd,
            default-mysql-server | virtual-mysql-server,
            inetutils-ping | iputils-ping,
            logrotate,
Suggests: cacti-spine,
          moreutils,
          snmpd,
Description: web interface for graphing of monitoring systems
 Cacti is a complete PHP-driven front-end for RRDTool. It stores all of
 the necessary data source information to create graphs, handles the data
 gathering, and populates the MySQL database with round-robin archives.
 It also includes SNMP support for those used to creating traffic graphs
 with MRTG.
 .
 This package requires a functional MySQL database server on either the
 installation host or a remotely accessible system.
